Format: 3.0 (quilt)
Source: demo-libuv
Binary: demo-libuv
Architecture: any
Version: 1.0-1
Maintainer: Filip Škola <filip@fqs.cz>
Standards-Version: 3.9.7
Build-Depends: debhelper (>= 9), libuv1
Package-List:
 demo-libuv deb misc optional arch=any
Checksums-Sha1:
 3b5e3d63261599152d88db019dde34a1d95a81a7 433 demo-libuv_1.0.orig.tar.gz
 6dfd8dfc94933846db9f78904448fb6eeb72957a 684 demo-libuv_1.0-1.debian.tar.xz
Checksums-Sha256:
 2ed7c3d2fc794a0883b4c2dafa7760fe28ba66f83083e6101986edb8c4000cb5 433 demo-libuv_1.0.orig.tar.gz
 6e4ff169e727f72d778a0d2a59e708b46bc4be1f0f15ec6872078470fa6dee71 684 demo-libuv_1.0-1.debian.tar.xz
Files:
 b312940acf21d96b39c8d77cab387bad 433 demo-libuv_1.0.orig.tar.gz
 fb460d880fd2ca0b23bd2e33878bd8b1 684 demo-libuv_1.0-1.debian.tar.xz
