Format: 3.0 (quilt)
Source: greet
Binary: greet
Architecture: all
Version: 1.0-1
Maintainer: Filip Škola <filip@fqs.cz>
Standards-Version: 3.9.7
Build-Depends: debhelper (>= 9)
Package-List:
 greet deb misc optional arch=all
Checksums-Sha1:
 144342ac19feae8f8fb84a0488f497fba75c7967 313 greet_1.0.orig.tar.gz
 c60d16ea2b2ac619789ed36aa55d0c0d5b110420 700 greet_1.0-1.debian.tar.xz
Checksums-Sha256:
 1430eeff608c909f2b5d597297f23335664acee49284685e4185179b56b1fa31 313 greet_1.0.orig.tar.gz
 f088728e133edeab259556f2edb147965b90abe5ed2d2ce6dab1b0ba15f07907 700 greet_1.0-1.debian.tar.xz
Files:
 73d74a7d1825a1c14d76e80e4fbdc6b0 313 greet_1.0.orig.tar.gz
 b4aa5d07adf78a2a6de700859451037a 700 greet_1.0-1.debian.tar.xz
