#!/bin/sh

ARG=$1
DATE=$(date +%A)
GREETING="Hello $(whoami), ${DATE}!"

if [ ! -z $ARG ] && [ $ARG -ge 0 ] 2>/dev/null
then
    for i in `seq 1 $ARG`
    do
        echo ${GREETING}
    done
else
    echo ${GREETING}
fi
